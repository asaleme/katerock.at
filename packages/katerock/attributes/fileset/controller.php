<?php 
namespace Concrete\Package\Katerock\Attribute\Fileset;

use Loader;
use FileSet;
use \Concrete\Core\Attribute\Controller as AttributeTypeController;

class Controller extends AttributeTypeController
{
    protected $searchIndexFieldDefinition = array('type' => 'integer', 'options' => array('default' => 0, 'notnull' => false));

    public function getValue()
    {
        $db = Loader::db();
        $value = $db->GetOne("select fsID from atFileset where avID = ?", array($this->getAttributeValueID()));
        if ($value > 0) {
            $fs = FileSet::getByID($value);

            return $fs;
        }
    }

    public function getSearchIndexValue()
    {
        $db = Loader::db();
        $value = $db->GetOne("select fsID from atFileset where avID = ?", array($this->getAttributeValueID()));
        return $value;
    }
    
    public function form()
    {
        $fsID = 0;
        if ($this->getAttributeValueID() > 0) {
            $fs = $this->getValue();
            $fsID = is_object($fs) ? $fs->getFileSetID() : 0;
        }

        $opts = array();
        $opts[0] = t("No fileset selected");
        
        $fsl = new \Concrete\Core\File\Set\SetList;
        foreach($fsl->get() AS $_fs) {
            $id = $_fs->getFileSetID();
            $opts[$id] = $_fs->getFileSetName();             
        }
        
        $fm = Loader::helper('form');
        $form = '<div class="ccm-attribute ccm-attribute-fileset">';
        $form .= $fm->select($this->field('value'),$opts,$fsID);
        $form .= '</div>';
        print $form;
    }

	public function saveValue($fsID) {
		$db = Loader::db();
		$fsID = ($fsID == false || $fsID == '0') ? 0 : $fsID;
		$db->Replace('atFileset', array('avID' => $this->getAttributeValueID(), 'fsID' => $fsID), 'avID', true);
	}

	public function saveForm($data) {
		$this->saveValue($data['value']);
	}
	
	public function deleteValue() {
		$db = Loader::db();
		$db->Execute('delete from atFileset where avID = ?', array($this->getAttributeValueID()));
	}
}
