<?php defined('C5_EXECUTE') or die("Access Denied.");

use \Concrete\Core\Area\Area;
/** @var Page $c */
$c = \Concrete\Core\Page\Page::getCurrentPage();

$date = $c->getCollectionDatePublicObject();
$month =  \Punic\Calendar::getMonthName($date, 'abbreviated', '', true);
$day = $date->format('d');
$vorschau = $c->getAttribute('thumbnail');
$meldung = $c->getAttribute('meldung');
$meldungContent = $c->getAttribute('blog_description');
?>
<?php $this->inc('elements/header.php'); ?>


<body id="top">

<div class="<?php echo $c->getPageWrapperClass()?>">

    <!-- ============================================================= MAIN ============================================================= -->

    <main class="main">

        <!-- ============================================================= SECTION – HERO ============================================================= -->

        <!-- ============================================================= SECTION – HERO : END ============================================================= -->

        <!-- ============================================================= HEADER ============================================================= -->


        <header>
            <div class="container">


                <div class="navbar">


                    <div class="navbar-header">
                        <div class="container">


                            <!-- ============================================================= LOGO MOBILE ============================================================= -->

                            <a class="navbar-brand" href="/"><img
                                    src="<?php echo $this->getThemePath(); ?>/images/art/kate-icon.png" class="logo" alt=""></a>

                            <!-- ============================================================= LOGO MOBILE : END ============================================================= -->

                            <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i
                                    class='icon-menu-1'></i></a>

                        </div>
                        <!-- /.container -->
                    </div>


                    <div class="yamm static">
                        <div class="navbar-collapse collapse">


                            <!-- ============================================================= MAIN NAVIGATION ============================================================= -->
                            <div class="nav-logo">
                                <a class="navbar-brand scroll-to" href="/#top"><img
                                        src="<?php echo $this->getThemePath(); ?>/images/art/kate-icon.png"
                                        class="logo" alt=""></a>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="visible-xs"><a href="/#top" class="scroll-to" data-anchor-offset="0">Home</a></li>

                                <li><a href="/#news" class="scroll-to" data-anchor-offset="0">News</a></li>
                                <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                                <li><a href="/#kursprogramm" class="scroll-to" data-anchor-offset="0">Kursprogramm</a></li>
                                <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                                <li><a href="/#preise" class="scroll-to" data-anchor-offset="0">Preise</a></li>
                                <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                                <li><a href="/#standorte" class="scroll-to" data-anchor-offset="0">Standorte</a></li>
                                <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                                <li><a href="/#katerock" class="scroll-to" data-anchor-offset="0">Katerock&nbsp;who?</a></li>


                            </ul>
                            <!-- /.nav -->

                            <!-- ============================================================= MAIN NAVIGATION : END ============================================================= -->


                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.yamm -->
                </div>
            </div>

            <!-- /.navbar -->
        </header>
        <!-- ============================================================= HEADER : END ============================================================= -->


        <!-- ============================================================= SECTION – PRODUCT ============================================================= -->

        <section id="blog-post">
            <div class="container inner-top-sm inner-bottom classic-blog dark-bg" style="margin-top: 64px; margin-bottom: 50px">
                <div class="row">

                    <div class="col-md-9 inner-right-sm">
                        <div class="sidemeta">

                            <div class="post kate format-gallery">

                                <div class="date-wrapper">
                                    <div class="date">
                                        <span class="day"><?php echo $day; ?></span>
                                        <span class="month"><?php echo $month; ?></span>
                                    </div>
                                    <!-- /.date -->
                                </div>
                                <!-- /.date-wrapper -->

                                <div class="post-content">

                                    <?php
                                    if(!$meldung) {
                                        ?>
                                        <div id="owl-work" class="owl-carousel owl-inner-pagination owl-inner-nav post-media">

                                            <?php

                                            $width = 750;
                                            $height = 900;
                                            $im = Loader::helper('image');



                                            /** @var \Concrete\Core\File\Set\Set $fileSet */
                                            $fileSet = $c->getAttribute('images');

                                            if($fileSet) {
                                                $files = \Concrete\Core\File\Set\File::getFileSetFiles($fileSet);
                                                foreach ($files as $file) {
                                                    $file = \Concrete\Core\File\File::getByID($file->fID)
                                                    ?>
                                                    <div class="item">
                                                        <figure>
                                                            <?php
                                                                $im->outputThumbnail($file, $width, $height, '', false, true);
                                                            ?>

                                                        </figure>
                                                    </div>
                                                    <?php
                                                }
                                            } else if ($vorschau){
                                                ?>

                                                <div class="item">
                                                    <figure>
                                                        <?php
                                                            $im->outputThumbnail($vorschau, $width, $height, '', false, true);
                                                        ?>

                                                    </figure>
                                                </div>

                                                <?php
                                            }

                                            ?>

                                        </div>
                                    <?php
                                    }

                                    ?>

                                    <!-- /.owl-carousel -->


                                    <?php

                                    if($meldung) {
                                        echo '<div id="owl-work" class="owl-carousel owl-inner-pagination owl-inner-nav post-media"></div>';
                                    }

                                    ?>


                                    <h1 class="post-title kate-color">
                                        <?php
                                        echo $c->getCollectionName();
                                        ?>
                                    </h1>

                                        <?php

                                        if(!$meldung) {
                                            $a = new Area('Inhalt');
                                            $a->display($c);

                                        } else {
                                            echo '<p>' . $meldungContent . '</p>';
                                        }



                                        ?>

                                </div>
                                <!-- /.post-content -->

                            </div>
                            <!-- /.post -->


                        </div>
                        <!-- /.sidemeta -->


                        <div class="sidemeta">

                            <?php

                            $a = new GlobalArea('Paginierung');
                            $a->display($c);

                            ?>
                            <!-- /.pagination -->

                        </div>


                    </div>
                    <!-- /.col -->

                    <aside class="col-md-3">
                        <?php
                        $a = new GlobalArea('aside');
                        $a->display($c);

                        ?>

                        <div class="sidebox widget">
                            <h4>Facebook</h4>

                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.8&appId=246526608721306";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>

                            <div class="fb-page" data-href="https://www.facebook.com/www.katerock.at/" data-small-header="true"
                                 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/www.katerock.at/" class="fb-xfbml-parse-ignore"><a
                                        href="https://www.facebook.com/www.katerock.at/">KateRock</a></blockquote>
                            </div>
                        </div>
                        <!-- /.widget -->


                    </aside>

                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>


</div>
<?php $this->inc('elements/footer.php'); ?>
