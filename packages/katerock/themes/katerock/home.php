<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php $this->inc('elements/header.php'); ?>

<body id="top">

<div class="<?php echo $c->getPageWrapperClass() ?>">

    <!-- ============================================================= MAIN ============================================================= -->

    <main class="main">

        <!-- ============================================================= SECTION – HERO ============================================================= -->


        <section id="hero" style="position: relative">


            <!-- <audio id="background_audio" autoplay="autoplay">
                 <source src="http://www.jplayer.org/audio/m4a/Miaow-07-Bubble.m4a"/>
             </audio>

             <a href="#noscroll" class="off" id="sound">/ sound <span id="music-text">on

         </span>
                 <i class="fa fa-volume-up" aria-hidden="true"></i>
                 <i class="fa fa-volume-off" aria-hidden="true"></i>
             </a>-->


            <a class="pull-right light-color fb-link"
               href="https://www.facebook.com/www.katerock.at/" target="_blank"><i style="font-size: 30px;"
                                                                                   class="icon-facebook-squared-1"></i></a>

            <a style="top: 40px" class="pull-right light-color fb-link"
               href="   https://www.instagram.com/katrin_katerock/" target="_blank"><i style="font-size: 30px;"
                                                                                   class="icon-s-instagram"></i></a>



            <img src="/application/files/8414/7800/9225/katerock_start.jpg" class="img-responsive"
                 style="width: 100%; height: auto;"/>

            <div class="container-fluid" style="position: absolute; top: 0; width: 100%; height: 100%;">

                <div style="position: relative;
    height: 100%;
    width: 100%;
    display: table;">


                    <div class="caption vertical-center text-center" style="margin: 0;
    display: table-cell;
    vertical-align: middle;
    padding: 20px;">

                        <img src="<?php echo $this->getThemePath(); ?>/images/art/katerock_logo.png"
                             class="fadeInDown-1 hero-logo" style="margin-top: -70px;"/>


                        <!-- /.fadeIn -->

                    </div>


                </div>

                <!-- /.caption -->
            </div>


        </section>
        <!-- ============================================================= SECTION – HERO : END ============================================================= -->

        <!-- ============================================================= HEADER ============================================================= -->


        <header>
            <div class="container">


                <div class="navbar">


                    <div class="navbar-header">


                        <!-- ============================================================= LOGO MOBILE ============================================================= -->

                        <a class="navbar-brand" href="#"><img
                                    src="<?php echo $this->getThemePath(); ?>/images/art/kate-icon.png" class="logo"
                                    alt=""></a>

                        <!-- ============================================================= LOGO MOBILE : END ============================================================= -->

                        <a class="btn responsive-menu pull-left" data-toggle="collapse"
                           data-target=".navbar-collapse"><i
                                    class='icon-menu-1'></i></a>


                    </div>


                    <div class="yamm">
                        <div class="navbar-collapse collapse sticky">


                            <!-- ============================================================= MAIN NAVIGATION ============================================================= -->
                            <div class="nav-logo">
                                <a class="navbar-brand scroll-to" href="#top"><img
                                            src="<?php echo $this->getThemePath(); ?>/images/art/kate-icon.png"
                                            class="logo" alt=""></a>
                            </div>
                            <ul class="nav navbar-nav">
                                <li class="visible-xs"><a href="#top" class="scroll-to" data-anchor-offset="0">Home</a>
                                </li>

                                <li><a href="#news" class="scroll-to" data-anchor-offset="0">News</a></li>
                                <li class="separator"><img
                                            src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png">
                                </li>
                                <li><a href="#kursprogramm" class="scroll-to" data-anchor-offset="0">Kursprogramm</a>
                                </li>
                                <li class="separator"><img
                                            src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png">
                                </li>
                                <li><a href="#preise" class="scroll-to" data-anchor-offset="0">Preise</a></li>
                                <li class="separator"><img
                                            src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png">
                                </li>
                                <li><a href="#standorte" class="scroll-to" data-anchor-offset="0">Standorte</a></li>
                                <li class="separator"><img
                                            src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png">
                                </li>
                                <li><a href="#katerock" class="scroll-to" data-anchor-offset="0">Katerock&nbsp;who?</a>
                                </li>


                            </ul>
                            <!-- /.nav -->

                            <!-- ============================================================= MAIN NAVIGATION : END ============================================================= -->


                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.yamm -->
                </div>
            </div>

            <!-- /.navbar -->
        </header>
        <!-- ============================================================= HEADER : END ============================================================= -->


        <!-- ============================================================= SECTION – PRODUCT ============================================================= -->
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-9 center-block text-center">
                    <header>
                        <img class="motto img-responsive"
                             src="<?php echo $this->getThemePath(); ?>/images/art/motto.png">
                    </header>
                </div>
                <!-- /.col -->
            </div>
        </div>

        <section id="news" class="dark-bg">
            <div class="container">


                <div class="row inner-sm news">
                    <?php
                    $a = new Area('News');
                    $a->display($c);

                    ?>


                    <div class="col-md-12 text-center" style="margin-bottom: -93px; margin-top: 30px;">
                        <h2>
                            <a class="light-color all-link" href="/blog">Alle Artikel</a>
                        </h2>

                        <div class="arrow-down center-block"></div>

                    </div>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </section>


        <section id="kursprogramm">
            <div class="container inner-top inner-bottom-xs">
                <h1 class="visible-xs light-color">Kurs&shy;programm</h1>

                <div class="row row-eq-height">
                    <div class="col-md-4 col-sm-12">
                        <div class="post format-standard">

                            <img class="star-icon" style="top: -17px"
                                 src="<?php echo $this->getThemePath(); ?>/images/art/icon_star.png"/>

                            <div class="post-content kate kate-color-a">

                                <?php
                                $a = new Area('Kurs_01');
                                $a->display($c);

                                ?>


                            </div>
                            <!-- /.post-content -->

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="post format-standard">
                            <img class="star-icon" style="top: -17px"
                                 src="<?php echo $this->getThemePath(); ?>/images/art/icon_star.png"/>


                            <div class="post-content kate kate-color-b">

                                <?php
                                $a = new Area('Kurs_02');
                                $a->display($c);

                                ?>


                            </div>
                            <!-- /.post-content -->

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="post format-standard">
                            <img class="star-icon" style="top: -17px"
                                 src="<?php echo $this->getThemePath(); ?>/images/art/icon_star.png"/>


                            <div class="post-content kate kate-color-c">
                                <?php
                                $a = new Area('Kurs_03');

                                $a->display($c);

                                ?>


                            </div>
                            <!-- /.post-content -->

                        </div>
                    </div>
                </div>


            </div>


        </section>

        <section id="preise">
            <div class="container inner-xs">
                <div class="row">

                    <div class="col-md-12">

                        <h1 class="visible-xs light-color">Preise</h1>

                        <div class="post format-standard">


                            <div class="post-content kate">
                                <div class="row">

                                    <div class="col-md-4">
                                        <?php
                                        $a = new Area('Preise_01');

                                        $a->display($c);

                                        ?>


                                    </div>
                                    <br class="visible-sm visible-xs"/><br class="visible-sm visible-xs"/>

                                    <div class="col-md-4 kate-color-c">

                                        <?php
                                        $a = new Area('Preise_02');

                                        $a->display($c);

                                        ?>


                                    </div>
                                    <br class="visible-sm visible-xs"/><br class="visible-sm visible-xs"/>

                                    <div class="col-md-4">

                                        <?php
                                        $a = new Area('Preise_03');

                                        $a->display($c);

                                        ?>


                                    </div>


                                </div>
                                <div class="clearfix"></div>


                            </div>
                            <!-- /.post-content -->

                        </div>

                    </div>


                </div>

            </div>
            <!-- /.container -->
        </section>


        <section id="standorte" class="dark-bg kate">
            <div class="container inner-xs">
                <h1 class="visible-xs light-color">Standorte</h1>

                <div class="row">
                    <div class="col-md-9 col-md-offset-1">

                        <?php
                        $a = new Area('Standorte');

                        $a->display($c);

                        ?>


                    </div>
                </div>

            </div>

        </section>


        <section id="katerock" class="img-bg-soft img-bg"
                 style="background-image: url(<?php echo $this->getThemePath(); ?>/images/art/kate.jpg); background-repeat: no-repeat; border-bottom: 4px solid #ffffff; border-top: 4px solid #ffffff">
            <div class="container">
                <div class="row">


                    <div class="col-md-6 col-sm-9 inner center-block text-center">
                        <div class="post format-standard" style="margin:80px 0 0px">


                            <div class="post-content kate">

                                <blockquote>
                                    <?php
                                    $a = new Area('Zitat');

                                    $a->display($c);

                                    ?>

                                </blockquote>

                            </div>
                            <!-- /.post-content -->

                        </div>


                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>

        <section>
            <div class="container inner">
                <div class="row light-color about">
                    <div class="col-md-5">
                        <img class="hidden-xs" style="position: absolute; left: -20px; margin-top: -20px;"
                             src="<?php echo $this->getThemePath(); ?>/images/art/icon_star.png"/>


                        <?php
                        $a = new Area('Katewho_01');

                        $a->display($c);

                        ?>


                        <br class="visible-xs">
                        <br class="visible-xs">
                    </div>

                    <div class="col-md-6 col-md-offset-1">


                        <?php
                        $a = new Area('Katewho_02');

                        $a->display($c);

                        ?>


                    </div>


                </div>
            </div>
        </section>
        <?php $this->inc('elements/footer.php'); ?>
