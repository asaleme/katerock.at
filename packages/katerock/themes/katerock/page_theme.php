<?php

namespace Concrete\Package\Katerock\Theme\Katerock;

use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;

class PageTheme extends \Concrete\Core\Page\Theme\Theme implements ThemeProviderInterface
{
    public function registerAssets()
    {
// PROVIDE
        // JS
        $this->providesAsset('css', 'bootstrap/*');
        $this->providesAsset('javascript-conditional', 'html5-shiv');
        $this->providesAsset('javascript-conditional', 'respond');

        // REQUIRE
        $this->requireAsset('javascript', 'jquery');
        $this->requireAsset('css', 'font-awesome');
        $this->requireAsset('javascript-conditional', 'html5-shiv');
        $this->requireAsset('javascript-conditional', 'respond');
    }

    protected $pThemeGridFrameworkHandle = 'bootstrap3';

    public function getThemeName()
    {
        return t('Katerock Theme');
    }

    public function getThemeDescription()
    {
        return t('ein Theme für Katerock');
    }

    public function getThemeBlockClasses()
    {
        return array(
  
        );
    }

    public function getThemeAreaClasses()
    {
        return array(
        );
    }

    public function getThemeDefaultBlockTemplates()
    {
        return array(
        );
    }

    public function getThemeResponsiveImageMap()
    {
        return array(
          
        );
    }

    public function getThemeEditorClasses()
    {
        return array(
           
        );
    }

    public function getThemeAreaLayoutPresets()
    {
        $presets = array(
            array(
               
            ),
            array(
               
            )
        );
        return $presets;
    }
}
