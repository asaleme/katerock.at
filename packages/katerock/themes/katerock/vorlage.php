<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php $this->inc('elements/header.php'); ?>




<body class="bg-set">

<!-- Root Container -->
<div id="root-container">


<!-- Header Container -->
<div id="header-container" class="main-width">

    <!-- Logo and Tagline -->
    <div id="logo-wrapper">
        <a href="/" class="bg-set"><img
                src="<?php echo $this->getThemePath(); ?>/images/armbruster-logo.png"
                alt="armbruster Isolierungen" title="armbruster Isolierungen"/></a>
    </div>
    <!--		<div id="tagline" class="bg-set">
                Working Professionally with Style
            </div>
            <div style="height: 400px"></div>

            -->

    <?php
    $a = new Area('Header');
    $a->enableGridContainer();
    $a->display($c);
    ?>





    <?php
    $a = new GlobalArea('Main-Nav');
    $a->enableGridContainer();
    $a->display($c);

    ?>


</div>
<!-- End id="header-container" -->

<hr class="line-style"/>

<div id="content-container">

    <hr id="dynamic-side-line" class="line-style"/>

    <div id="inner-content-container" class="main-width">

        <!-- Page Intro -->
        <div id="intro-wrapper">


            <?php
            $a = new Area('Intro');
            $a->display($c);
            ?>


        </div>
        <!-- End id="intro-wrapper" -->

        <div id="content-wrapper" class="with-sidebar">

            <?php
            $a = new Area('Main');
            $a->enableGridContainer();
            $a->display($c);
            ?>


        </div>
        <!-- End id="content-wrapper" -->

        <!-- Sidebar -->
        <div id="sidebar-wrapper">


            <?php
            $a = new Area('Sidebar');
            $a->display($c);
            ?>


        </div>
        <!-- End id="sidebar-wrapper" -->

    </div>
    <!-- End id="inner-content-container" -->

</div>
<!-- End id="content-container" -->

<!--<hr class="line-style"/>-->

<!-- Footer Section -->
<div id="footer-root-container">

    <!-- Footer Content -->
    <!--<div id="footer-content-container" class="main-width">
        <div id="footer-content" class="row no-margin-bottom">
            <div class="large-3 columns">
                <div class="contact-widget">
                    <h5>The Thine</h5>

                    <p>
                        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                        accusantium doloremque laudantium
                    </p>

                    <p>
                        <a class="button"><i class="icon ion-ios7-download-outline"></i>Buy on ThemeForest</a>
                    </p>
                </div>
            </div>
            <div class="large-3 columns">
                <h5>Contact Info</h5>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do. Suspendisse ac varius diam.
                </p>

                <p>
                    456 Fifth Avenue, 39th fl. NY, US
                </p>
                <ul class="contact">
                    <li>
                        Phone: +123.456.789
                    </li>
                    <li>
                        Mobile: +123.999.899
                    </li>
                    <li>
                        Email: <a href="#">contact@thine.com</a>
                    </li>
                </ul>
            </div>
            <div class="large-3 columns">
                <div class="posts-widget">
                    <h5>Recent News</h5>
                    <ul>
                        <li>
                            <div class="post-title">
                                <a href="#">Auis aute irure dolor in reprehen derit in voluptate</a>
                                <span class="date">July 18, 2014</span>
                            </div>
                        </li>

                        <li>
                            <div class="post-title">
                                <a href="#">Duis aute irure dolor in reprehen</a>
                                <span class="date">June 29, 2014</span>
                            </div>
                        </li>

                        <li>
                            <div class="post-title">
                                <a href="#">Oaio dignissimos ducimus qui blanditiis praesentium.</a>
                                <span class="date">June 15, 2014</span>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="large-3 columns">
                <h5>Tags</h5>
                <ul class="tags-widget">
                    <li><a href="#">building</a></li>
                    <li><a href="#">interior</a></li>
                    <li><a href="#">office</a></li>
                    <li><a href="#">resort</a></li>
                    <li><a href="#">technology</a></li>
                </ul>
            </div>
        </div>
    </div>-->

    <hr class="line-style"/>

    <!-- Footer Bar -->
    <div id="footer-bar-container" class="main-width">
        <div id="footer-bar">
            <a id="footer-logo" href="/"><img
                    src="<?php echo $this->getThemePath(); ?>/images/armbruster-logo.png"
                    alt="Armbruster Isolierungen" title="Armbruster Isolierungen"/></a>




            <?php
            $a = new GlobalArea('Footer');
            $a->enableGridContainer();
            $a->display($c);

            ?>


        </div>
    </div>

</div>

</div>
<!-- End id="root-container" -->
<?php $this->inc('elements/footer.php'); ?>


</body>
</html>
