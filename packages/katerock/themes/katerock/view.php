<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php $this->inc('elements/header.php');
use \Concrete\Core\Area\Area;
?>

<body>
<div class="<?php echo $c->getPageWrapperClass()?>">


    <!-- ============================================================= HEADER ============================================================= -->

    <header>


        <div class="container">


            <div class="navbar">


                <div class="navbar-header">
                    <div class="container">


                        <!-- ============================================================= LOGO MOBILE ============================================================= -->

                        <a class="navbar-brand" href="/"><img src="<?php echo $this->getThemePath(); ?>/images/art/kate-icon.png" class="logo" alt=""></a>

                        <!-- ============================================================= LOGO MOBILE : END ============================================================= -->

                        <a class="btn responsive-menu pull-right" data-toggle="collapse" data-target=".navbar-collapse"><i
                                    class='icon-menu-1'></i></a>

                    </div>
                    <!-- /.container -->
                </div>


                <div class="yamm static">
                    <div class="navbar-collapse collapse">


                        <!-- ============================================================= MAIN NAVIGATION ============================================================= -->
                        <div class="nav-logo">
                            <a class="navbar-brand scroll-to" href="/#top"><img src="<?php echo $this->getThemePath(); ?>/images/art/kate-icon.png"
                                                                                class="logo" alt=""></a>
                        </div>
                        <ul class="nav navbar-nav">
                            <li class="visible-xs"><a href="/#top" class="scroll-to" data-anchor-offset="0">Home</a></li>

                            <li><a href="/#news" class="scroll-to" data-anchor-offset="0">News</a></li>
                            <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                            <li><a href="/#kursprogramm" class="scroll-to" data-anchor-offset="0">Kursprogramm</a></li>
                            <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                            <li><a href="/#preise" class="scroll-to" data-anchor-offset="0">Preise</a></li>
                            <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                            <li><a href="/#standorte" class="scroll-to" data-anchor-offset="0">Standorte</a></li>
                            <li class="separator"><img src="<?php echo $this->getThemePath(); ?>/images/art/nav_star.png"></li>
                            <li><a href="/#katerock" class="scroll-to" data-anchor-offset="0">Katerock&nbsp;who?</a></li>


                        </ul>
                        <!-- /.nav -->

                        <!-- ============================================================= MAIN NAVIGATION : END ============================================================= -->


                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.yamm -->
            </div>


        </div>

        <!-- /.navbar -->

        <a style="display: none" class="pull-right light-color fb-link"
           href="https://www.facebook.com/www.katerock.at/" target="_blank"><i style="font-size: 30px;"
                                                                               class="icon-facebook-squared-1"></i></a>

    </header>

    <!-- ============================================================= HEADER : END ============================================================= -->


    <!-- ============================================================= MAIN ============================================================= -->

    <main class="main">


        <!-- ============================================================= SECTION – BLOG ============================================================= -->

        <section id="blog">
            <div class="container inner-top-xs inner-bottom classic-blog dark-bg"
                 style="margin-top: 64px; margin-bottom: 50px">


                <div class="row">

                    <div class="col-md-9 inner-right-sm">

                        <?php
                        $a = new Area('Blogeinträge');
                        $a->display($c);

                        ?>




                    </div>
                    <!-- /.col -->

                    <aside class="col-md-3">
                        <?php
                        $a = new GlobalArea('aside');
                        $a->display($c);

                        ?>


                        <div class="sidebox widget">
                            <h4>Facebook</h4>

                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/de_DE/sdk.js#xfbml=1&version=v2.8&appId=246526608721306";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>

                            <div class="fb-page" data-href="https://www.facebook.com/www.katerock.at/"
                                 data-small-header="true"
                                 data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote cite="https://www.facebook.com/www.katerock.at/" class="fb-xfbml-parse-ignore">
                                    <a
                                            href="https://www.facebook.com/www.katerock.at/">KateRock</a></blockquote>
                            </div>
                        </div>
                        <!-- /.widget -->


                    </aside>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </section>

        <!-- ============================================================= SECTION – BLOG : END ============================================================= -->
        <?php $this->inc('elements/footer.php'); ?>


