<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>

<html lang="<?php echo Localization::activeLanguage()?>">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">



    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $this->getThemePath(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Customizable CSS -->

    <link href="<?=$view->getStylesheet('style.less')?>" rel='stylesheet' type='text/css' data-skrollr-stylesheet>
    <link href="<?php echo $this->getThemePath(); ?>/css/main.css" rel="stylesheet">

    <link href="<?php echo $this->getThemePath(); ?>/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo $this->getThemePath(); ?>/css/owl.transitions.css" rel="stylesheet">
    <link href="<?php echo $this->getThemePath(); ?>/css/animate.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Bungee|Roboto:400,700,900" rel="stylesheet">

    <!-- Icons/Glyphs -->
    <link href="<?php echo $this->getThemePath(); ?>/fonts/fontello.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo $this->getThemePath(); ?>/images/favicon.ico">

    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
    <script src="<?php echo $this->getThemePath(); ?>/js/html5shiv.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/respond.min.js"></script>
    <![endif]-->

    <?php Loader::element('header_required');?>


    <meta property="og:title" content="KateRock">
    <meta property="og:url" content="http://katerock.at">
    <meta property="og:image" content="http://katerock.at/600x600.png">
    <meta property="og:image" content="http://katerock.at/1200x630.png">


    <script src="<?php echo $this->getThemePath(); ?>/js/cookieconsent.min.js"></script>

    <script>
        window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#574092",
                        "text": "#fff"
                    },
                    "button": {
                        "background": "#d4144a",
                        "text": "#fff"
                    }
                },
                "position": "top",
                "static": true,
                "content": {
                    "message": "Ich verwende Cookies, um dir das beste Nutzererlebnis bieten zu können. Durch die weitere Nutzung der Webseite stimmst du der Verwendung von Cookies zu. ",
                    "dismiss": "Akzeptieren und fortfahren",
                    "link": "Weitere Informationen",
                    "href": "/datenschutz"
                }
            })});
    </script>

</head>