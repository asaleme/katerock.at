<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<!-- ============================================================= FOOTER ============================================================= -->

<footer class="dark-bg">
    <div class="container">
        <div class="row">

            <div class="col-sm-3 inner">

                <?php
                $a = new GlobalArea('Footer_01');
                $a->display($c);

                ?>


            </div>
            <!-- /.col -->


            <!-- /.col -->


            <div class="col-sm-4 inner light-color">
                <?php
                $a = new GlobalArea('Footer_02');
                $a->display($c);
                ?>




                <!-- /.contacts -->
            </div>
            <!-- /.col -->

            <div class="col-sm-5 col-sm-6 inner">
                <?php
                $a = new GlobalArea('Footer_03');
                $a->display($c);

                ?>


            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->
    </div>
    <!-- .container -->

</footer>


<div class="container inner-xs" style="position: relative">
    <a class="herzvollkreativ" href="http://herzvoll-kreativ.at/" target="_blank"><img src="<?php echo $this->getThemePath(); ?>/images/art/websignet-herzvollkreativ.png"/>
    </a>

    <div class="row">
        <div class="col-md-2 col-xs-6 col-sm-3 center-block">
            <img src="<?php echo $this->getThemePath(); ?>/images/art/katerock_logo.png"
                 class="img-responsive text-center fadeInDown-1"/>
        </div>
    </div>
</div>


</main>

<!-- ============================================================= MAIN : END ============================================================= -->


<!-- ============================================================= FOOTER : END ============================================================= -->

</div>

<!-- JavaScripts placed at the end of the document so the pages load faster -->

<?php

if ($c->isEditMode()) {
    ?>

    <script src="<?php echo $this->getThemePath(); ?>/js/scripts-editmode.js"></script>

<?php
} else {
    ?>

    <script src="<?php echo $this->getThemePath(); ?>/js/jquery.easing.1.3.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/bootstrap-hover-dropdown.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/skrollr.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/skrollr.stylesheets.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/waypoints.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/waypoints-sticky.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/imagesloaded.pkgd.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/jquery.easytabs.min.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/viewport-units-buggyfill.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/equal.js"></script>
    <script src="<?php echo $this->getThemePath(); ?>/js/scripts.js"></script>

<?php
}


?>



<?php Loader::element('footer_required'); ?>

</body>
</html>