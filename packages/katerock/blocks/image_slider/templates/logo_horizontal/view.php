<?php defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Core\Page\Page;
$c = Page::getCurrentPage();

/** @var Page $c */
$c = Page::getCurrentPage();
if ($c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item" style="<?php echo isset($width) ? "width: $width;" : '' ?><?php echo isset($height) ? "height: $height;" : '' ?>">
        <div style="padding: 40px 0px 40px 0px"><?php echo t('Image Slider disabled in edit mode.')?></div>
    </div>
<?php  } else { ?>
    <!-- SLIDER -->



    <div id="owl-related-posts" class="owl-carousel owl-item-gap <?php echo $bID ?>">

        <?php
        foreach($rows as $row) {
            $f = File::getByID($row['fID']); ?>
        <div class="item">
                <a target="_blank" href="<?php echo $row['linkURL'] ?>">

                    <?php if(is_object($f)) {

                        $ih = Core::make('helper/image');
                        $image = $ih->getThumbnail($f, 400, 300, false); // set false if you don't want it cropped

                        echo '<img class="img-responsive inline" src="' . $image->src  . '" alt="' . $row['title'] .  '"  />';

                    }?>
                </a>
        </div>
        <?php } ?>
    </div>
<?php } ?>

