<div class="posts kate sidemeta">

    <?php

    foreach ($pages as $page) {
        /** @var \Concrete\Core\Page\Page $page */
        $date = $page->getCollectionDatePublicObject();
        $name = $page->getCollectionName();
        /** @var \Concrete\Core\File\File $vorschau */
        $vorschau = $page->getAttribute('thumbnail');
        $description = $page->getAttribute('blog_description');
        $link = $page->getCollectionPath();
        $meldung = $page->getAttribute('meldung');

        $month =  \Punic\Calendar::getMonthName($date, 'abbreviated', '', true);
        $day = $date->format('d');
        if($meldung) {
            ?>
            <div class="post format-link">

                <div class="date-wrapper">
                    <div class="date">
                        <span class="day"><?php echo $day?></span>
                        <span class="month"><?php echo $month?></span>
                    </div>
                    <!-- /.date -->
                </div>
                <!-- /.date-wrapper -->


                <div class="post-content">
                    <h2 class="post-title">
                        <a href="javascript:void(0)" target="_blank"><?php echo $name?></a>
                    </h2>

                       <?php echo $description ?>
                    </div>
                <!-- /.post-content -->

            </div>

            <?php
        } else if($vorschau) {
            ?>

            <div class="post format-image">

                <div class="date-wrapper">
                    <div class="date">
                        <span class="day"><?php echo $day?></span>
                        <span class="month"><?php echo $month?></span>
                    </div>
                    <!-- /.date -->
                </div>
                <!-- /.date-wrapper -->

                <div class="post-content">

                    <figure class="icon-overlay icn-link post-media">
                        <a href="<?php echo $link;?>"><img src="<?php echo $vorschau->getRecentVersion()->getRelativePath() ?>" alt=""></a>
                    </figure>
                    <!-- /.post-media -->

                    <h2 class="post-title">
                        <a href="<?php echo $link;?>">
                            <?php echo $name?>
                        </a>
                    </h2>

                   <?php echo $description ?>

                    <a href="<?php echo $link;?>" class="btn">weiterlesen</a>

                </div>
                <!-- /.post-content -->

            </div>

            <?php
        } else {
            ?>
            <div class="post format-standard">

                <div class="date-wrapper">
                    <div class="date">
                        <span class="day"><?php echo $day?></span>
                        <span class="month"><?php echo $month?></span>
                    </div>
                    <!-- /.date -->
                </div>
                <!-- /.date-wrapper -->

                <div class="post-content">

                    <h2 class="post-title">
                        <a href="<?php echo $link ?>">
                            <?php echo $name?>
                        </a>
                    </h2>


                  <?php echo $description ?>

                    <a href="<?php echo $link;?>" class="btn">weiterlesen</a>
                </div>
                <!-- /.post-content -->

            </div>

            <?php
        }

    }




    ?>



</div>
<!-- /.posts -->


<?php if ($showPagination): ?>
    <?php echo $pagination;?>
<?php endif; ?>
