<div class="col-md-6 visible-xs">
    <h1 class="visible-xs light-color">News</h1>

</div>

<?php

/** @var \Concrete\Core\Utility\Service\Text $textHelper */
$textHelper = Core::make('helper/text');

foreach ($pages as $page) {
    /** @var \Concrete\Core\Page\Page $page */
    $date = $page->getCollectionDatePublicObject();
    $name = $page->getCollectionName();
    /** @var \Concrete\Core\File\File $vorschau */
    $vorschau = $page->getAttribute('thumbnail');
    $description = $page->getAttribute('blog_description');
    $link = $page->getCollectionPath();
    $meldung = $page->getAttribute('meldung');

    $month = \Punic\Calendar::getMonthName($date, 'wide', '', false);
    $day = $date->format('d');
    $year = $date->format('Y');

    if ($meldung) {
        $type = 'Meldung';
    } else {
        $type = 'News';
    }
    ?>
    <div class="col-md-6 <?php echo strtolower($type);?>">

        <div class="post format-standard">
            <img class="star-icon" src="/packages/katerock/themes/katerock/images/art/icon_star.png"/>


            <div class="post-content kate light-color">

                <h2 class="post-title light-color">
                    <?php echo $name;?>
                </h2>

                <ul class="meta date">
                    <li class="date"><?php echo $type;?> vom  <?php echo $day . '. ' . $month .'. '. $year ?></li>
                </ul>
                <!-- /.meta -->

                <?php echo $textHelper->shorten($description, 140)?>

                <?php

                if($type == 'News') {
                    echo '<a href="' . $link . '" class="btn pull-right kate">/ weiterlesen</a>';
                }

                ?>


                <div class="clearfix"></div>

            </div>
            <!-- /.post-content -->

        </div>
    </div>


    <?php
}
?>

